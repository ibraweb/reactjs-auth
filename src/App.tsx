import axios from 'axios';
import { useEffect, useState } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import Nav from './components/Nav';
import Forgot from './pages/Forgot';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Reset from './pages/Reset';

function App() {
  const [user, setUser] = useState(null);
  const [login, setLogin] = useState(false);
   useEffect(() => {
        (
            async () => {
                
                try {
                    const response = await axios.get('user');

                    const user = response.data;
                   
                   setUser(user)
                   
                } catch (e) {
                      setUser(null)
                }

                
                
            }
        )();
    }, [login])

  return (
    <div className="App">
      <BrowserRouter>
      <Nav  user={user} setLogin = {() => setLogin(false) } />
         <Route path="/" exact component={() => <Home user={setUser(user)}/>} />
         <Route path="/login" exact component={() => <Login setLogin={() => setLogin(true)} /> } />
         <Route path="/register" exact component={Register} />
         <Route path="/forgot" exact component={Forgot} />
         <Route path="/reset/:token" exact component={Reset} />
    </BrowserRouter>
   </div>
  );
}

export default App;
