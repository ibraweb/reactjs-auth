import axios from 'axios';
import React, { SyntheticEvent, useState } from 'react';
import { Link } from 'react-router-dom';

const Forgot = () => {

    const [email, setEmail] = useState('');

    const [notify, setNotify] = useState({
        show: false,
        error: false,
        messgae: ''
        
    });


    const submit = async (e: SyntheticEvent) => {
        e.preventDefault();
        try {
            await axios.post('forgot', { email })
            setNotify({
                show: true,
                error: false,
                messgae: 'Email was send success!'
            })
        } catch (e) {
            setNotify({
                show: true,
                error: true,
                messgae: 'Email does not exist!'
            });
            
        }

    }

    let info;

    if (notify.show) {
        info = (
            <div className={notify.error ? 'alert alert-danger': 'alert alert-success'} role="alert">
                        {notify.messgae}
            </div>
        )
    }

    return (
    
    <form className="form-signin" onSubmit={submit}>
            <h1 className="h3 mb-3 font-weight-normal">Please put your email</h1>
             <div className="container">
                <input type="email" className="form-control mb-3" placeholder="Email" required
                onChange={e => setEmail(e.target.value)}
                />

                <button className="btn btn-lg btn-primary btn-block" type="submit">Send email</button>
            </div>   
         
        </form>
        
    );
};

export default Forgot;