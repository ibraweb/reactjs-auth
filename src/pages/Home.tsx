import React from "react";

const Home = ({ user }: { user: any }) => {
    let message;

    if (user) {
          message = `Hi ${user.firt_name} ${user.last_name}`
    } else {
        message = 'You are not logged in!'
    }
    
    return (
        <div className='container'>
            { message }
        </div>
    );
};

export default Home;