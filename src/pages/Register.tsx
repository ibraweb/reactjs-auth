import React, { SyntheticEvent, useState } from 'react';
import axios from 'axios'
import { Redirect } from 'react-router-dom';

const Register = () => {
    const [firt_name, setfirt_name] = useState('');
    const [last_name, setlast_name] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password_confirm, setpassword_confirm] = useState('');
    const [redirect, setRedirect] = useState(false)

   
    const submit = async (e: SyntheticEvent) => {
        e.preventDefault()

        await axios.post('register', {
            firt_name: firt_name,
            last_name: last_name,
            email: email,
            password: password,
            password_confirm: password_confirm
        })
        //console.log('Data:', response);

        setRedirect(true)
        
    }
    
    if (redirect) {
        return <Redirect to="/login" />
    }
     
    return (
        <form className="form-signin" onSubmit={submit}>
            <h1 className="h3 mb-3 font-weight-normal">Please register</h1>
            
            <input className="form-control" placeholder='First Name' required
             onChange={e => setfirt_name(e.target.value)}
            />
            
            <input className="form-control" placeholder='Last Name' required
                onChange={e => setlast_name(e.target.value)}
            />
            
            <input type="email" className="form-control" placeholder='Email' required
                 onChange={e => setEmail(e.target.value)}
            />

            
            <input type="password" className="form-control" placeholder="Password" required
                 onChange={e => setPassword(e.target.value)}
            />

            <input type="password" className="form-control" placeholder="Password Confirm" required
                 onChange={e => setpassword_confirm(e.target.value)}
            />

            
            <button className="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        </form>
    );
};

export default Register;